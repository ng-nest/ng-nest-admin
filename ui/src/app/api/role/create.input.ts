export class CreateRoleInput {
  name!: string;
  description?: string;
}
