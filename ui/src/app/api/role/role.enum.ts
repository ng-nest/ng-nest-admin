export enum RoleDescription {
  Role = '角色',

  Name = '角色名称',
  Description = '角色描述'
}

export enum RoleResolverName {
  Role = '角色详情',
  Roles = '角色列表',
  CreateRole = '创建角色',
  UpdateRole = '更新角色',
  DeleteRole = '删除角色'
}

export enum RoleMessage {
  CreatedSuccess = '新增角色成功！',
  UpdatedSuccess = '更新角色成功！',
  DeletedSuccess = '删除角色成功！'
}
