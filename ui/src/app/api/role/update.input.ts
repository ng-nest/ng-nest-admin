export class UpdateRoleInput {
  name?: string;
  description?: string;
}
