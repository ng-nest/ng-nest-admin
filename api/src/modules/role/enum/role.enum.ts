export enum RoleDescription {
  Role = '角色',

  Name = '角色名称',
  Description = '角色描述'
}

export enum RoleIncludeDescription {
  RoleUser = '角色用户'
}

export enum RoleResolverName {
  Role = '角色详情',
  Roles = '角色列表',
  RoleSelect = '角色查询（没有分页）',
  CreateRole = '创建角色',
  UpdateRole = '更新角色',
  DeleteRole = '删除角色'
}
