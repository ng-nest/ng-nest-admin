export * from './create.input';
export * from './update.input';
export * from './role-order';
export * from './role-pagination.input';
export * from './role-where';
export * from './role.output';
export * from './role-select.output';
